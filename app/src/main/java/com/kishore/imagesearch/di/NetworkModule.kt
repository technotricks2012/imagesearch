package com.kishore.imagesearch.di

import android.content.Context
import com.google.gson.GsonBuilder
import com.kishore.imagesearch.data.network.Api
import com.kishore.imagesearch.data.network.NetworkConnectionInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object NetworkModule {
    @Singleton
    @Provides
    fun provideRecipeNetworkConnectionInterceptor(@ApplicationContext appContext: Context): NetworkConnectionInterceptor = NetworkConnectionInterceptor(appContext)

    @Singleton
    @Provides
    fun provideRecipeOkHttpClient(networkConnectionInterceptor: NetworkConnectionInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(networkConnectionInterceptor)
            .build()
    }

    @Singleton
    @Provides
    fun provideRecipeApi(okkHttpClient : OkHttpClient): Api {
        return Retrofit.Builder()
            .client(okkHttpClient)
            .baseUrl(Api.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
            .create(Api::class.java)
    }


}