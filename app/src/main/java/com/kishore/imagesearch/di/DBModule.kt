package com.kishore.imagesearch.di

import android.content.Context
import com.kishore.imagesearch.data.db.AppDatabase
import com.kishore.imagesearch.data.db.dao.ImageSearchDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DBModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase = AppDatabase(appContext)

    @Provides
    @Singleton
    fun provideImageSearchDao(appDatabase: AppDatabase): ImageSearchDao {
        return appDatabase.getImageSearchDao()
    }


}