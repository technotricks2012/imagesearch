package com.kishore.imagesearch.di

import com.kishore.imagesearch.data.db.dao.ImageSearchDao
import com.kishore.imagesearch.data.network.Api
import com.kishore.imagesearch.data.repository.ImageRepository
import com.kishore.imagesearch.data.repository.ImageRepositoryImpl
import com.kishore.imagesearch.data.repository.WebRepository
import com.kishore.imagesearch.data.repository.WebRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun provideRecipeRepository(
        service: Api,
        dao: ImageSearchDao
    ): ImageRepository {
        return ImageRepositoryImpl(service,dao)
    }

    @Singleton
    @Provides
    fun provideRecipeWebViewRepository(
    ): WebRepository {
        return WebRepositoryImpl()
    }
}