package com.kishore.imagesearch.data.network.response

import com.kishore.imagesearch.model.Image

data class ImageSearchResponse(
    val type: String,
    val totalCount: Long,
    val value: List<Image>
)