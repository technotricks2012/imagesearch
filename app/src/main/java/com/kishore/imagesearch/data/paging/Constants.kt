package com.kishore.imagesearch.data.paging


object Constants {
    const val STARTING_PAGE_INDEX = 1
    const val NETWORK_PAGE_SIZE = 10
}