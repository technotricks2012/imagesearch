package com.kishore.imagesearch.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.*
import com.kishore.imagesearch.data.db.dao.ImageSearchDao
import com.kishore.imagesearch.data.network.Api
import com.kishore.imagesearch.data.paging.Constants
import com.kishore.imagesearch.data.paging.ImagesRemoteMediator
import com.kishore.imagesearch.model.Image
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


class ImageRepositoryImpl @Inject constructor(
    private val service: Api,
    private val dao: ImageSearchDao
):ImageRepository {


    override fun getImagesFlow(query: String): Flow<PagingData<Image>> {
        Log.d("ImageRepository", "New query: $query")

        // appending '%' so we can allow other characters to be before and after the query string
        val dbQuery = "%${query.replace(' ', '%')}%"
        val pagingSourceFactory = { dao.getImages(dbQuery) }

        @OptIn(ExperimentalPagingApi::class)
        return Pager(
            config = PagingConfig(
                pageSize = Constants.NETWORK_PAGE_SIZE,
                enablePlaceholders = true
            ),
            remoteMediator = ImagesRemoteMediator(
                query,
                service,
                dao
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }

    override fun getImagesLiveData(query: String): LiveData<PagingData<Image>> {
        Log.d("ImageRepository", "New query: $query")

        // appending '%' so we can allow other characters to be before and after the query string
        val dbQuery = "%${query.replace(' ', '%')}%"
        val pagingSourceFactory = { dao.getImages(dbQuery) }

        @OptIn(ExperimentalPagingApi::class)
        return Pager(
            config = PagingConfig(
                pageSize = Constants.NETWORK_PAGE_SIZE,
                enablePlaceholders = true
            ),
            remoteMediator = ImagesRemoteMediator(
                query,
                service,
                dao
            ),
            pagingSourceFactory = pagingSourceFactory
        ).liveData
    }

}