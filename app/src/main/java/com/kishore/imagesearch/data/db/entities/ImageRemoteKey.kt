package com.kishore.imagesearch.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ImageRemoteKey(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val prev: Int?,
    val next: Int?
)