package com.kishore.imagesearch.data.db.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kishore.imagesearch.data.db.entities.ImageRemoteKey
import com.kishore.imagesearch.model.Image


@Dao
interface ImageSearchDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<Image>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSingle(image: Image)

    @Query(  "Select * From Image Where title LIKE :query")
    fun getImages(query :String): PagingSource<Int, Image>

    @Query("DELETE FROM Image")
    suspend fun deleteAllImages()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllKeys(list: List<ImageRemoteKey>)

    @Query("SELECT * FROM ImageRemoteKey WHERE id = :id")
    suspend fun getAllKey(id: String): ImageRemoteKey?

    @Query("DELETE FROM ImageRemoteKey")
    suspend fun deleteAllKeys()

}