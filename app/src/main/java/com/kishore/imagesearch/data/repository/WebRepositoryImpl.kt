package com.kishore.imagesearch.data.repository

import android.webkit.URLUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData


class WebRepositoryImpl:WebRepository{
    override fun getWeb(query: String): LiveData<String> {
        val _url =  MutableLiveData<String>()
        if(URLUtil.isValidUrl(query)){
            _url.value = query
        }
        else{
            _url.value = "https://www.google.com/search?q=$query"
        }
        return _url
    }
}