package com.kishore.imagesearch.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kishore.imagesearch.data.db.dao.ImageSearchDao
import com.kishore.imagesearch.data.db.entities.ImageRemoteKey
import com.kishore.imagesearch.model.Image


@Database(
    entities = [
        Image::class,
        ImageRemoteKey::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase: RoomDatabase() {

    abstract fun getImageSearchDao(): ImageSearchDao
    companion object{
        @Volatile
        private var INSTANCE:AppDatabase?=null

        private val LOCK = Any()
        operator  fun invoke(context: Context) = INSTANCE?: synchronized(LOCK){
            INSTANCE?:buildDatabase(context).also {
                INSTANCE = it
            }
        }

        private fun buildDatabase(context: Context)=
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "ImageSearch.db"
            ).build()
    }
}