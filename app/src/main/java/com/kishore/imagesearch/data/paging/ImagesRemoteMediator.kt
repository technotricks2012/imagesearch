package com.kishore.imagesearch.data.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.kishore.imagesearch.data.db.dao.ImageSearchDao
import com.kishore.imagesearch.data.db.entities.ImageRemoteKey
import com.kishore.imagesearch.data.network.Api
import com.kishore.imagesearch.model.Image
import retrofit2.HttpException
import java.io.IOException


@OptIn(ExperimentalPagingApi::class)
class ImagesRemoteMediator(
    private val query: String,
    private val service: Api,
    private val dao: ImageSearchDao
) : RemoteMediator<Int, Image>() {

    override suspend fun initialize(): InitializeAction =
        InitializeAction.LAUNCH_INITIAL_REFRESH

    override suspend fun load(loadType: LoadType, state: PagingState<Int, Image>): MediatorResult {

        val page = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.next?.minus(1) ?: Constants.STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)
                val prevKey = remoteKeys?.prev
                if (prevKey == null) {
                    return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                }
                prevKey
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                val nextKey = remoteKeys?.next
                if (nextKey == null) {
                    return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
                }
                nextKey
            }
        }

        val apiQuery = query

        try {
            val apiResponse = service.getImages(apiQuery, page, state.config.pageSize)

            val images = apiResponse.body()?.value ?: emptyList()
            val endOfPaginationReached = images.isEmpty()

            // clear all tables in the database
            if (loadType == LoadType.REFRESH) {
//                    dao.deleteAllKeys()
//                    dao.deleteAllImages()
            }
            val prevKey = if (page == Constants.STARTING_PAGE_INDEX) null else page - 1
            val nextKey = if (endOfPaginationReached) null else page + 1
            val keys = images.map {
                ImageRemoteKey(it.title, prevKey, nextKey)
            }
            dao.insertAllKeys(keys)
            dao.insert(images)


            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, Image>): ImageRemoteKey? {

        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { repo ->
                dao.getAllKey(repo.title)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, Image>): ImageRemoteKey? {

        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { repo ->
                dao.getAllKey(repo.title)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, Image>
    ): ImageRemoteKey? {

        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.title?.let { repoId ->
                dao.getAllKey(repoId)
            }
        }
    }
}