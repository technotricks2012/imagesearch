package com.kishore.imagesearch.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.kishore.imagesearch.data.network.Api
import com.kishore.imagesearch.model.Image
import retrofit2.HttpException
import java.io.IOException


class ImagePagingSource(
    private val service: Api,
    private val query: String
) : PagingSource<Int, Image>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Image> {
        val position = params.key ?: Constants.STARTING_PAGE_INDEX
        val apiQuery = query
        return try {
            val response = service.getImages(apiQuery, position, params.loadSize)
            val repos = response.body()?.value?: emptyList()
            val nextKey = if (repos.isEmpty()) {
                null
            } else {
                // initial load size = 3 * NETWORK_PAGE_SIZE
                // ensure we're not requesting duplicating items, at the 2nd request
                position + (params.loadSize / Constants.NETWORK_PAGE_SIZE)
            }
            LoadResult.Page(
                data = repos,
                prevKey = if (position == Constants.STARTING_PAGE_INDEX) null else position - 1,
                nextKey = nextKey
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }
    override fun getRefreshKey(state: PagingState<Int, Image>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

}