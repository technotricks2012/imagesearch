package com.kishore.imagesearch.data.network

import com.kishore.imagesearch.data.network.response.ImageSearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
import com.kishore.imagesearch.BuildConfig


interface Api {
    @Headers(
        "x-rapidapi-key: $CLIENT_ID",
        "x-rapidapi-host: contextualwebsearch-websearch-v1.p.rapidapi.com"
    )
    @GET("Search/ImageSearchAPI")
    suspend fun getImages(
        @Query("q") query: String,
        @Query("pageNumber") page: Int,
        @Query("pageSize") perPage: Int
    ): Response<ImageSearchResponse>


    companion object{
        const val BASE_URL = BuildConfig.BASE_URL
        const val CLIENT_ID = BuildConfig.RAPID_API_ACCESS_KEY
    }
}