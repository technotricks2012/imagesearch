package com.kishore.imagesearch.data.repository

import androidx.lifecycle.LiveData

interface WebRepository{
    fun getWeb(query: String): LiveData<String>
}