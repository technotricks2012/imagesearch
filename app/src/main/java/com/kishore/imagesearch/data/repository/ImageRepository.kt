package com.kishore.imagesearch.data.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.kishore.imagesearch.model.Image
import kotlinx.coroutines.flow.Flow


interface ImageRepository{
    fun getImagesFlow(query: String): Flow<PagingData<Image>>
    fun getImagesLiveData(query: String): LiveData<PagingData<Image>>
}