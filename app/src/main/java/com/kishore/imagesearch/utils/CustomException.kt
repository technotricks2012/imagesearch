package com.kishore.imagesearch.utils

import java.io.IOException


open class NoInternetException(message : String = "") : IOException(message)

open class ApiException(message : String = "") : IOException(message)