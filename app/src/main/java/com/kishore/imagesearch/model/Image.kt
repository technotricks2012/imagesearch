package com.kishore.imagesearch.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity
data class Image(
    var title: String,
    var name: String? = null,
    var url: String? = null,
    var height: Long? = null,
    var width: Long? = null,
    var thumbnail: String? = null,
    var thumbnailHeight: Long? = null,
    var thumbnailWidth: Long? = null,
    var imageWebSearchUrl: String? = null,
    var webpageUrl: String? = null,
): Parcelable {
    @PrimaryKey(autoGenerate = true)
    var uid = 0
}

