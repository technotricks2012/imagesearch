package com.kishore.imagesearch.ui.tab

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.kishore.imagesearch.R
import com.kishore.imagesearch.databinding.FragmentTabBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class TabFragment : Fragment(R.layout.fragment_tab) {
    private lateinit var tabAdapter: TabsPagerAdapter

    private var _binding: FragmentTabBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        (activity as AppCompatActivity).supportActionBar?.show()

        _binding = FragmentTabBinding.bind(view)
        tabAdapter = TabsPagerAdapter(this)
        binding.viewpager.adapter = tabAdapter

        TabLayoutMediator(
            binding.tabLayout, binding.viewpager
        ) { tab, position ->
            tab.text = when (position) {
                0 -> getString(R.string.images)
                1 -> getString(R.string.webView)
                else -> ""
            }
        }.attach()
    }
}