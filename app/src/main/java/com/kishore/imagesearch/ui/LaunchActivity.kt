package com.kishore.imagesearch.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.kishore.imagesearch.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LaunchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
    }
}