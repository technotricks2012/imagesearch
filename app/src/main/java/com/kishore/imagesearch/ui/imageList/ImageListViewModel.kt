package com.kishore.imagesearch.ui.imageList

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.kishore.imagesearch.data.repository.ImageRepository


class ImageListViewModel @ViewModelInject constructor(
    private val repository: ImageRepository,
    @Assisted state: SavedStateHandle
) : ViewModel() {
    private val currentQuery = state.getLiveData(CURRENT_QUERY, DEFAULT_QUERY)

    val images = currentQuery.switchMap { queryString ->
        repository.getImagesLiveData(queryString)
            .cachedIn(viewModelScope)
    }

    fun searchQuery(query: String) {
        currentQuery.value = query
    }

    companion object {
        private const val CURRENT_QUERY = "current_query"
        private const val DEFAULT_QUERY = "cats"
    }


}