package com.kishore.imagesearch.ui.tab

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.kishore.imagesearch.ui.imageList.ImageListFragment
import com.kishore.imagesearch.ui.websearch.WebSearchFragment


class TabsPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2
    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ImageListFragment()
            1 -> WebSearchFragment()
            else -> WebSearchFragment()
        }
    }
}