package com.kishore.imagesearch.ui.websearch

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.kishore.imagesearch.data.repository.WebRepository

class WebSearchViewModel @ViewModelInject constructor(
    private val repository: WebRepository,
    @Assisted state: SavedStateHandle
) : ViewModel() {
    private val currentQuery = state.getLiveData(CURRENT_QUERY, DEFAULT_QUERY)

    val url = currentQuery.switchMap { queryString ->
        repository.getWeb(queryString)
    }

    fun searchQuery(query: String) {
        currentQuery.value = query
    }

    companion object {
        private const val CURRENT_QUERY = "current_query"
        private const val DEFAULT_QUERY = "https://www.google.com"
    }


}