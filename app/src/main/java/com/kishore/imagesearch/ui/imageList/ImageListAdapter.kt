package com.kishore.imagesearch.ui.imageList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.kishore.imagesearch.R
import com.kishore.imagesearch.databinding.ItemImageBinding
import com.kishore.imagesearch.model.Image


class ImageListAdapter(private val listener: OnItemClickListener) :
    PagingDataAdapter<Image, ImageListAdapter.PhotoViewHolder>(PHOTO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val binding =
            ItemImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return PhotoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val currentItem = getItem(position)

        if (currentItem != null) {
            holder.bind(currentItem, position)
        }
    }

    inner class PhotoViewHolder(private val binding: ItemImageBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    if (item != null) {
                        listener.onItemClick(item)
                    }
                }
            }
        }

        fun bind(photo: Image, position: Int) {
            binding.apply {
                Glide.with(itemView)
                    .load(photo.thumbnail)
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .error(R.drawable.ic_error)
                    .placeholder(R.drawable.ic_placeholder)
                    .into(imageView)

                textViewUserName.text = "${position+1} ${photo.title}"
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(photo: Image)
    }

    companion object {
        private val PHOTO_COMPARATOR = object : DiffUtil.ItemCallback<Image>() {
            override fun areItemsTheSame(oldItem: Image, newItem: Image) =
                oldItem.title == newItem.title

            override fun areContentsTheSame(oldItem: Image, newItem: Image) =
                oldItem == newItem
        }
    }
}