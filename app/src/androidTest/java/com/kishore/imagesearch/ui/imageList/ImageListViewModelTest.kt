package com.kishore.imagesearch.ui.imageList

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import androidx.paging.AsyncPagingDataDiffer
import com.google.common.truth.Truth
import com.kishore.imagesearch.data.db.dao.FakeImageSearchDao
import com.kishore.imagesearch.data.db.dao.ImageSearchDao
import com.kishore.imagesearch.data.network.Api
import com.kishore.imagesearch.data.network.FakeMyApi
import com.kishore.imagesearch.data.repository.ImageRepository
import com.kishore.imagesearch.data.repository.ImageRepositoryImpl
import com.kishore.imagesearch.helper.PHOTO_COMPARATOR
import com.kishore.imagesearch.helper.getOrAwaitValue
import com.kishore.imagesearch.helper.noopListUpdateCallback
import com.kishore.imagesearch.model.Image
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
class ImageListViewModelTest {


    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var dao: ImageSearchDao
    private lateinit var service: Api
    private lateinit var viewModel: ImageListViewModel
    private lateinit var repo: ImageRepository
    private lateinit var savedStateHandle: SavedStateHandle

    @Before
    fun setUp() {
        savedStateHandle  = SavedStateHandle()

        dao = FakeImageSearchDao(listOf(Image("test")))
        service = FakeMyApi()
        repo= ImageRepositoryImpl(service,dao)
        viewModel = ImageListViewModel(repo, savedStateHandle)
    }

    @Test
    fun imageListTest()= runBlockingTest(testDispatcher) {
        val differ = AsyncPagingDataDiffer(
            diffCallback = PHOTO_COMPARATOR,
            updateCallback = noopListUpdateCallback,
            mainDispatcher = testDispatcher,
            workerDispatcher = testDispatcher,
        )
        val job = launch {
            viewModel.searchQuery("test")
            val data = viewModel.images.getOrAwaitValue()
            differ.submitData(data)
        }
        advanceUntilIdle()
        Truth.assertThat(differ.snapshot().items.size).isEqualTo(0)
        job.cancel()
    }
}