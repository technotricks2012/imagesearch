package com.kishore.imagesearch.ui.web_search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import com.google.common.truth.Truth
import com.kishore.imagesearch.data.repository.WebRepository
import com.kishore.imagesearch.data.repository.WebRepositoryImpl
import com.kishore.imagesearch.helper.MainCoroutineRule
import com.kishore.imagesearch.helper.getOrAwaitValue
import com.kishore.imagesearch.ui.websearch.WebSearchViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
class WebSearchViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: WebSearchViewModel

    private lateinit var repo: WebRepository

    private lateinit var savedStateHandle: SavedStateHandle

    @Before
    fun setUp() {
        savedStateHandle  = SavedStateHandle()
        repo = WebRepositoryImpl()
        viewModel = WebSearchViewModel(repo, savedStateHandle)
    }

    @Test
    fun webRepositoryUrlTest() = runBlockingTest {
        viewModel.searchQuery("Android Developer")
        val result = viewModel.url.getOrAwaitValue ()
        Truth.assertThat(result).startsWith("https://")
    }
}