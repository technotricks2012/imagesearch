package com.kishore.imagesearch.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.AsyncPagingDataDiffer
import com.google.common.truth.Truth
import com.kishore.imagesearch.data.db.dao.FakeImageSearchDao
import com.kishore.imagesearch.data.db.dao.ImageSearchDao
import com.kishore.imagesearch.data.network.Api
import com.kishore.imagesearch.data.network.FakeMyApi
import com.kishore.imagesearch.helper.PHOTO_COMPARATOR
import com.kishore.imagesearch.helper.noopListUpdateCallback
import com.kishore.imagesearch.model.Image
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
class ImageRepositoryImplTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var dao: ImageSearchDao
    private lateinit var service: Api
    private lateinit var repo:ImageRepository

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)

        dao = FakeImageSearchDao(listOf(Image("test")))
        service = FakeMyApi()
        repo= ImageRepositoryImpl(service,dao)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun imageRepositoryTest()= runBlockingTest(testDispatcher) {
        val differ = AsyncPagingDataDiffer(
            diffCallback = PHOTO_COMPARATOR,
            updateCallback = noopListUpdateCallback,
            mainDispatcher = testDispatcher,
            workerDispatcher = testDispatcher,
        )
        val job = launch {
            repo.getImagesFlow("test").collectLatest {
                differ.submitData(it)
            }
        }
        advanceUntilIdle()
        Truth.assertThat(differ.snapshot().items.size).isGreaterThan(0)
        job.cancel()
    }
}
