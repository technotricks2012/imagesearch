package com.kishore.imagesearch.data.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kishore.imagesearch.data.db.dao.ImageSearchDao
import com.kishore.imagesearch.data.db.entities.ImageRemoteKey
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AppDatabaseTest : TestCase() {

    private lateinit var db: AppDatabase
    private lateinit var dao: ImageSearchDao
    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room
            .inMemoryDatabaseBuilder(context, AppDatabase::class.java)
            .build()

        dao = db.getImageSearchDao()
    }

    @Test
    fun insertKeyInsert() = runBlocking {
        val key =  ImageRemoteKey("test",null,1)
        dao.insertAllKeys(listOf(key))
        val result = dao.getAllKey(key.id)
        assertTrue(result?.id == key.id)
    }
}