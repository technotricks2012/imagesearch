package com.kishore.imagesearch.data.network

import com.kishore.imagesearch.data.network.response.ImageSearchResponse
import com.kishore.imagesearch.model.Image
import retrofit2.Response

class FakeMyApi:Api {
    override suspend fun getImages(
        query: String,
        page: Int,
        perPage: Int
    ): Response<ImageSearchResponse> = Response.success(getImageSearchResponse( query))


    private  fun getImageSearchResponse(  query: String): ImageSearchResponse {
        val images = listOf(

            Image(title = "$query Image 1",name = "Name 1"),
            Image(title = "$query Image 2",name = "Name 1"),
            Image(title = "$query Image 3",name = "Name 1"),
            Image(title = "$query Image 4",name = "Name 1"),
            Image(title = "$query Image 5",name = "Name 1"),
            Image(title = "$query Image 6",name = "Name 1"),
            Image(title = "$query Image 7",name = "Name 7"),
            Image(title = "$query Image 8",name = "Name 8"),
            Image(title = "$query Image 9",name = "Name 9"),
            Image(title = "$query Image 10",name = "Name 10")
        )
        return ImageSearchResponse("images",images.size.toLong(),images)
    }

}