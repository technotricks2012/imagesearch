package com.kishore.imagesearch.data.db.dao

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.kishore.imagesearch.data.db.AppDatabase
import com.kishore.imagesearch.data.db.entities.ImageRemoteKey
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ImageSearchDaoTest : TestCase() {

    private lateinit var db: AppDatabase
    private lateinit var dao:ImageSearchDao

    @Before
    public override fun setUp() {
        db = Room.inMemoryDatabaseBuilder(ApplicationProvider.getApplicationContext(), AppDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        dao = db.getImageSearchDao()
    }
    @After
    public override fun tearDown() {
        db.close()
    }

    @Test
    fun insertKeyInsert()= runBlocking {
        val key =  ImageRemoteKey("test",null,1)
        dao.insertAllKeys(listOf(key))
        val result = dao.getAllKey(key.id)
        assertTrue(result?.id == key.id)
    }
}