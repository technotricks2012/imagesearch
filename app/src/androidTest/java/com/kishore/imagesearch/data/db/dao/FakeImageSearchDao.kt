package com.kishore.imagesearch.data.db.dao

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.kishore.imagesearch.data.db.entities.ImageRemoteKey
import com.kishore.imagesearch.model.Image

class FakeImageSearchDao(val images: List<Image>):ImageSearchDao {
    override suspend fun insert(list: List<Image>) {
    }

    override suspend fun insertSingle(image: Image) {
    }

    override fun getImages(query: String): PagingSource<Int, Image> {
        return object : PagingSource<Int, Image>() {
            override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Image> {
                return LoadResult.Page(
                    data = images,
                    prevKey = null,
                    nextKey = null,
                )
            }

            // Ignored in test.
            override fun getRefreshKey(state: PagingState<Int, Image>): Int? = null
        }    }

    override suspend fun deleteAllImages() {
    }

    override suspend fun insertAllKeys(list: List<ImageRemoteKey>) {}

    override suspend fun getAllKey(id: String): ImageRemoteKey? =
        ImageRemoteKey("id",null,null)

    override suspend fun deleteAllKeys() {
    }
}