package com.kishore.imagesearch.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import com.kishore.imagesearch.helper.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class WebRepositoryImplTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var repo:WebRepository

    @Before
    fun setUp() {
        repo= WebRepositoryImpl()
    }

    @Test
    fun webRepositoryTest()= runBlockingTest {
        val result = repo.getWeb("Android Developer").getOrAwaitValue()

        Truth.assertThat(result).startsWith("https://")
    }
}